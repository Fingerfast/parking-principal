import React, {useState} from 'react'
import styled from 'styled-components'
import Theme from './Theme'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import ParkingLotsList from './containers/ParkingLotsList/ParkingLotsList'
import ParkingLotDetail from './containers/ParkingLotDetail/ParkingLotDetail'
import ApiKeyInput from "./containers/ApiKeyInput/ApiKeyInput";
import AppContext from "./appContext";

const Body = styled.div`
  display: flex;
  flex: 1 0 auto;
  flex-direction: column;
  background-color: ${props => props.theme.colors.darkGrey}
`;
const Header = styled.div`
  flex: 0 0 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-transform: uppercase;
  color: tomato;
`;
const Content = styled.div`
  display:flex;
  justify-content: center;
  background-color: ${props => props.theme.colors.white};
  padding: 2em;
`;
const Footer = styled.div`
  flex: 1 0 auto;
  display:flex;
  padding: 1em;
  justify-content: center;
  margin-top: 2em;
  background-color: ${props => props.theme.colors.darkGrey}
`;

function App() {
  return (
    <Theme>
      <Router>
        <Switch>
          <Route path='/404'>
            <div>Page not found</div>
          </Route>
          <Route path='/:id' exact>
            <Body>
              <Header>Detail parkoviště</Header>
              <Content>
                <ParkingLotDetail />
              </Content>
            </Body>
          </Route>
          <Route path='/'>
            <Body>
              <Header>Seznam parkovacích míst</Header>
              <Content>
                <ParkingLotsList />
              </Content>
              <Footer>
                <ApiKeyInput />
              </Footer>
            </Body>
          </Route>
        </Switch>
      </Router>
    </Theme>
  );
}

function ContextProvider() {
  const [context, setContext] = useState("");
  return (
      <AppContext.Provider value={[context, setContext]}>
        <App/>
      </AppContext.Provider>
  );
}

export default ContextProvider;

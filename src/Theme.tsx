import React from "react";
import { ThemeProvider } from "styled-components";
const theme = {
  colors: {
    darkGrey: "#243647",
    lightBlue: "#B9D2DC",
    brown: "#c76e4f",
    pastelBlue: "#b9d2dc",
    white: "#fff",
    yellow: "#FFBC5D"
  },
  paperCard: {
    background: "#fffef4",
    stackedBackground: "#efeddc"
  },
  fontSizes: {
    small: "1em",
    medium: "2em",
    large: "3em"
  }
};
const Theme = ({ children }: any) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);
export default Theme;
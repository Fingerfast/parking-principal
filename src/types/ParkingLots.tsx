export interface ParkingLots {
    type: string
    features: []
}

export interface ParkingLot {
    properties: {
        id: number
        name: string
        address: {
            address_formatted: string
        }
        parking_type: {
            description: string
        }
        num_of_free_places: number
        total_num_of_places: number
    };
}
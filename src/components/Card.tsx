import styled from "styled-components";

const Card = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 10px;
  box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.5);
  padding: 2em;
  cursor: pointer;
`

export default Card;
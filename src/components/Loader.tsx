import React from "react";
import ReactLoading from "react-loading";
import styled from "styled-components";

const CircleLoader = styled(ReactLoading)`
  align-self: center;
`

const Loader: React.FC<{}> = (props) => {
    return (
        <CircleLoader type="spin" width={200} height={200} color="tomato"/>
    );
};

export default Loader;
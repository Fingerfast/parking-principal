import React from "react";

type Props = [string, any]

const AppContext = React.createContext<Partial<Props>>([]);

export default AppContext
import React, {useCallback} from 'react';
import styled from "styled-components";
import useParkinglots from '../../services/useParkingLots';
import {useHistory} from 'react-router-dom'
import { ParkingLot } from '../../types/ParkingLots';
import Loader from "../../components/Loader";
import ParkingPlaceCard from '../ParkingPlaceCard/ParkingPlaceCard'

const Root = styled.div`
  max-width: 1230px;
  text-align: center;
  align-self: center;
`
const ListWrapper = styled.div`
  justify-content: center;
  display: flex;
  flex-flow: row wrap;
  margin-left: -1em;
  margin-bottom: -1em;
`

const ParkingLotsList: React.FC<{}> = () => {
  const service = useParkinglots();
  const history = useHistory()

  const clickOnParkingDetail = useCallback((id: number) => () => {
    history.push(`/${id}`)
  }, [history])

  return (
    <Root>
      <ListWrapper>
        {service.status === 'loading' && <Loader/>}
        {service.status === 'loaded' &&
        //TODO: show any UI message for no data in database...
        service.payload?.features?.map((parkingLot: ParkingLot, index: number) => {
          return (
              <ParkingPlaceCard
                  key={`${parkingLot.properties.name}-${index}`}
                  title={parkingLot.properties.name}
                  freePlaces={parkingLot.properties.num_of_free_places}
                  totalPlaces={parkingLot.properties.total_num_of_places}
                  address={parkingLot.properties.address.address_formatted}
                  handleClick={clickOnParkingDetail}
                  id={parkingLot.properties.id}
              />
          )
        })
        }
        {service.status === 'error' && (
            <div>Error, the backend moved to the dark side.</div>
        )}
      </ListWrapper>
    </Root>
  );
};

export default ParkingLotsList;
import React, {Fragment} from 'react';
import styled from "styled-components";
import Card from "../../components/Card";
import Loader from "../../components/Loader";
import useLot from "../../services/useLot";
import parkingIcon from "../../assets/icons/parking.svg";

const Root = styled(Card)`
  cursor: default;
  max-width: 1004px;
`
const Name = styled.div`
  font-size: 2em;
  color: tomato;
`

const ParkingLotDetail: React.FC<{}> = () => {
    const service = useLot();

    return (
        <Root>
            {
                service.status === 'loading' && <Loader/>
            }
            {
                service.status === 'loaded' &&
                  <Fragment>
                    <Name>{service.payload.properties.name}</Name>
                    <img src={parkingIcon} width={100} height={100} alt={"Parking place"}/>
                    <div>{service.payload.properties.parking_type.description}</div>
                    <p>{service.payload.properties.address.address_formatted}</p>
                  </Fragment>
            }
            {
                service.status === 'error' && (
                    <div>Error, the backend moved to the dark side.</div>
                )
            }
        </Root>
    );
};

export default ParkingLotDetail;
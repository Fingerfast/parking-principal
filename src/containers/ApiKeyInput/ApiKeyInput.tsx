import React, { useCallback, useState, useContext } from 'react';
import styled from "styled-components";
import AppContext from "../../appContext";

const Root = styled.div`
  cursor: default;
  max-width: 1004px;
  align-self: center;
  flex-direction: column;
  display:flex;
  text-align: center;
  color: white;
  textarea {
    margin: 2em;
  }
`

const Button = styled.div`
  border-radius: 5px;
  background-color: tomato;
  padding: 1em;
  width: auto;
`


const ApiKeyInput: React.FC<{}> = () => {
    const [apiKey, setApiKey] = useState('')
    const [context, setContext] = useContext(AppContext)
    console.log('---context', context)

    const handleInputKey = useCallback((e: any) => {
        setApiKey(e.target.value)
    },[setApiKey])

    const clickOnSave = useCallback(() => {
        setApiKey(apiKey)
        setContext(apiKey)
    },[apiKey,setContext,setApiKey])

    return (
        <Root>
            <div>Pro zobrazení reálných dat, vložte zde vygenerovaný "API klíč"</div>
            <textarea value={apiKey} onChange={handleInputKey} rows={5} cols={80}/>
            <Button onClick={clickOnSave}>Uložit</Button>
        </Root>
    );
};

export default ApiKeyInput;
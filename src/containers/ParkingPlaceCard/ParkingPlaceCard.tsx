import React from 'react';
import styled from "styled-components";
import parkingIcon from '../../assets/icons/parking.svg'
import Card from '../../components/Card'

const Root = styled.div`
  max-width: 350px;
  margin-right: 1em;
  margin-top: 1em;
`
const Title = styled.div`
  font-size: 1.5em;
  margin-bottom: 1em;
  text-transform: uppercase;
`
const BasicInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 1em;
  font-weight: 600;
  font-size: 1.4em;
`
const FreeNumber = styled.div`
  color: tomato;
  margin-right: 5px;
`
const Address = styled.div`
  font-size: 1em;
`
const AvailablePlaces = styled.div`
  display: flex;
  color: gray;
  margin-top: 1em;
  align-items: center;
`
const Occupancy = styled.div`
  display: flex;
`

interface CardProps {
    title: string
    address: string
    freePlaces: number
    totalPlaces: number
    handleClick: (id: number) => () => void
    id: number
}

const ParkingPlaceCard: React.FC<CardProps> = ({title, address, freePlaces, totalPlaces, handleClick, id}: CardProps) => {
    return (
        <Root onClick={handleClick(id)}>
            <Card>
                <Title>{title}</Title>
                <img src={parkingIcon} width={100} height={100} alt={"Parking place"}/>
                <BasicInfo>
                    <Address>{address}</Address>
                    <AvailablePlaces>počet volných míst: </AvailablePlaces>
                    <Occupancy><FreeNumber>{freePlaces}</FreeNumber>/ {totalPlaces}</Occupancy>
                </BasicInfo>
            </Card>
        </Root>
    );
};

export default ParkingPlaceCard;
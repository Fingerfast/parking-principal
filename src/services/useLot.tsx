import {useState, useEffect, useContext} from 'react'
import { ParkingLot } from '../types/ParkingLots'
import { Service } from '../types/Service'
import {useHistory} from 'react-router-dom'
import AppContext from "../appContext";

const mockServer = 'https://private-c72b2-golemioapi.apiary-mock.com/v2/parkings/'
const prodServer = 'https://api.golemio.cz/v2/parkings/'

export default function useLot() {
    const [lot, setLot] = useState<Service<ParkingLot>>({
        status: 'loading'
    })

    //TODO: this logic should be outside wrapper for all calls which needed access token
    const [contextApiKey] = useContext(AppContext)
    const history = useHistory()
    let slugId = history.location.pathname.split('/').pop()
    const url = contextApiKey !== '' ? `${prodServer}${slugId}` : `${mockServer}${slugId}`
    const [headers] = useState(new Headers())
    headers.set("x-access-token", `${contextApiKey}`);
    headers.set("Content-Type", "application/json");

    useEffect(() => {
        fetch(url, {
            method: 'GET',
            headers: headers
        })
          .then(response => response.json())
          .then(response => {
            console.log('DETAIL LOT: ' , response)
            setLot({ status: 'loaded', payload: response })
          })
          .catch(error => setLot({ status: 'error', error }));
      }, [url, headers]);

    return lot
}
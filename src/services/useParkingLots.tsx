import {useState, useEffect, useContext} from 'react'
import { ParkingLots } from '../types/ParkingLots'
import { Service } from '../types/Service'
import AppContext from "../appContext";

const mockServer = 'https://private-c72b2-golemioapi.apiary-mock.com/v2/parkings/'
const prodServer = 'https://api.golemio.cz/v2/parkings/'

export default function useParkingLots() {
    const [parkingLots, setParkingLots] = useState<Service<ParkingLots>>({
        status: 'loading'
    })

    //TODO: this logic should be outside wrapper for all calls which needed access token
    const [contextApiKey] = useContext(AppContext)
    const [headers] = useState(new Headers())
    const url = contextApiKey !== '' ? prodServer : mockServer
    headers.set("x-access-token", `${contextApiKey}`);
    headers.set("Content-Type", "application/json");

    useEffect(() => {
        fetch(url, {
            method: 'GET',
            headers: headers
        })
          .then(response => response.json())
          .then(response => {
            setParkingLots({ status: 'loaded', payload: response })
          })
          .catch(error => setParkingLots({ status: 'error', error }));
      }, [contextApiKey, url, headers]);

    return parkingLots
}